# -*- coding: utf-8 -*-


"""
unittests for spider_web/crawler.py
"""


import unittest

import requests
from ddt import data, ddt, unpack

from src.core.crawler import Crawler


@ddt
class TestCrawler(unittest.TestCase):

    @unittest.skip('dublicated')
    @data('http://google.com/', 'http://twitter.com/', 'http://facebook.com/')
    def test_do_request(self, url):
        """
        internet connection test
        """

        raised = False
        value = None

        try:
            value = Crawler.do_request(url)
        except:
            raised = True

        self.assertFalse(raised, 'Exception raised')
        self.assertIsNotNone(value)

    @unpack
    @data([Crawler.do_request(url='http://lenta.ru'), 'http://lenta.ru'], [Crawler.do_request(url='http://soccer.ru/'), 'http://soccer.ru'])
    def test_get_links(self, response, base_url):
        crawler = Crawler()
        links = crawler.get_links(response, base_url)
        self.assertTrue(type(links) is list)
        for link in links[::10]:
            response = requests.get(link)
            if response.status_code != 200:
                print(link)
                self.assertNotEqual(response.status_code, 500)
                self.assertNotEqual(response.status_code, 404)


if __name__ == '__main__':
    unittest.main()
