# -*- coding: utf-8 -*-

import threading


class Consumer(threading.Thread):

    """
    frontier_queue: queue with tasks
    output_queue: queue with crawling results (content, new links)
    crawler: specific crawler instance
    """

    def __init__(self, frontier_queue, output_queue,  crawler):
        threading.Thread.__init__(self)
        self.frontier_queue = frontier_queue
        self.output_queue = output_queue

        # crawler instance
        self.crawler = crawler

    def run(self):
        """
        run consumer loop, while not read 'quit'
        from frontier queue
        """
        while True:

            if not self.frontier_queue.empty():

                task = self.frontier_queue.get()
                if isinstance(task, str) and task == 'quit':
                    break

                try:
                    response = self.crawler.run(task.url)
                    task.links = response['links']
                    self.output_queue.put(task)
                except Exception as e:
                    print(str(e))

                try:
                    self.frontier_queue.task_done()
                except AttributeError:
                    pass
