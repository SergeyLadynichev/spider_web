# -*- coding: utf-8 -*-


class Task:

    def __init__(self, item, deep_value=None):
        """
        :param item: task from seeds.json or producer
        :param deep: weight of parent task
        """
        self.url = item
        self.deep = deep_value if deep_value else 1
        self.links = []

    def __str__(self):
        return "{},  deep:{}".format(self.url, self.deep)

    def check_deep(self, max_deep):
        """
        :param max_deep: weight of task (max link deep). from settings.json
        """
        return True if self.deep < max_deep and self.deep + 1 <= max_deep\
            else False
