# -*- coding: utf-8 -*-

"""
unittests for spider_web/helpers.py
"""
import json
import os.path
import unittest

from ddt import ddt, data


@ddt
class TestHelpers(unittest.TestCase):

    @data('/../settings.json', '/../seeds.json')
    def test_read_file(self, value):
        with open(os.path.dirname(__file__) + value, "r") as f:
            result = f.read()

        try:
            item = json.loads(result)
        except Exception as e:
            raise Exception

        self.assertNotEquals(0, len(item))

    def test_singleton(self):
        from src.core.helpers import singleton
        fake_class = type('FakeClass', (object, ), {})
        A = singleton(fake_class)
        obj1 = A()
        obj2 = A()
        self.assertEqual(id(obj1), id(obj2))


if __name__ == '__main__':
    unittest.main()
