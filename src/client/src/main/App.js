import React, { Component } from 'react';
import './styles/App.css';
import Table from './components/table'


class MainTable extends Component {
  render() {
    return (
      <div className="App">
        <p className="App-intro">
          <Table />
        </p>
      </div>
    );
  }
}

export default MainTable;
