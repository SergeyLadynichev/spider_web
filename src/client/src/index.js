import React from 'react';
import ReactDOM from 'react-dom';
import './main/styles/index.css';
import MainTable from './main/App';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<MainTable />, document.getElementById('root'));
registerServiceWorker();
