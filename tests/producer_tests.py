# -*- coding: utf-8 -*-

"""
unittests for spider_web/producer.py
"""

import unittest

from ddt import ddt, data

from src.core.producer import Producer


@ddt
class ProducerTests(unittest.TestCase):

    @data(2)
    def test_build_worker_pool(self, size):
        print("producer test passed")
        pr = Producer()
        workers = pr.build_worker_pool(size)
        self.assertEqual(len(workers), size)
