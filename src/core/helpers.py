# -*- coding: utf-8 -*-

import json
import time
import functools


def singleton(class_):
    """singleton decorator for producer instance"""
    instances = {}

    def get_instance(*args, **kwargs):
        if class_ not in instances:
            instances[class_] = class_(*args, **kwargs)
        return instances[class_]

    return get_instance


def get_cpu_count():
    """return system cores count. Used in worker pool builder"""
    import multiprocessing
    return multiprocessing.cpu_count()


def read_file(file_path):
    with open(file_path, 'r') as file:
        data = json.load(file)
        return data


def profiler(func):
    start_time = time.time()

    @functools.wraps(func)
    def inner(*args, **kwargs):
        return func(*args, **kwargs)

    print('All jobs done! Time taken: {}'.format(time.time() - start_time))
    return inner


def memoize(func):
    memo = []

    @functools.wraps(func)
    def inner(*args, **kwargs):
        if args[1] in memo:
            pass
        else:
            memo.append(args[1])
            return func(*args, **kwargs)
    return inner

