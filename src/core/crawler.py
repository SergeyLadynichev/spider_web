# -*- coding: utf-8 -*-

from urllib.parse import urljoin
from urllib import request, error
import lxml.html as lh


class Crawler:

    def __init__(self):
        pass

    def get_links(self, response, base_url):
        """parse links from response"""
        root = lh.fromstring(response)
        links_lxml_res = root.cssselect("a")
        links = [link.get("href") for link in links_lxml_res
                 if link.get("href") is not None]
        links = [link for link in links if not link.startswith('mailto')]
        return [urljoin(base_url, link) if not link.startswith('http') else link for link in links]

    @staticmethod
    def do_request(url):
        """make http web request to target url"""
        try:
            req = request.Request(url)
            response = request.urlopen(req, timeout=5).read()
            return response
        except (error.URLError, error.ContentTooShortError,
                 error.HTTPError) as e:
            print(e)
            raise error.HTTPError(url, 'code?', url, 'headers?', 'fp?')

    def run(self, url):
        """
        make http request and return to consumer
        :param url: target url for crawler
        :return: task
        """
        response = self.do_request(url)
        links = self.get_links(response, url)

        return {
            'url': url,
            'links': links
        }
