# -*- coding: utf-8 -*-

"""
unittests for spider_web/consumer.py
"""

import unittest
from multiprocessing import Queue

from crawler import Crawler
from ddt import ddt, data

from src.core.consumer import Consumer


@ddt
class ConsumerTests(unittest.TestCase):

    @data('http://www.1tv.ru')
    def test_run(self, fake_task):
        frontier_queue = Queue()
        output_queue = Queue()
        crawler = Crawler()
        frontier_queue.put(fake_task)
        frontier_queue.put('quit')
        cn = Consumer(frontier_queue, output_queue, crawler)
        self.assertEqual(cn.run(), None)


if __name__ == '__main__':
        unittest.main()


