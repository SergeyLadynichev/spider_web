# -*- coding: utf-8 -*-

import argparse

from helpers import read_file
from producer import Producer


def main():
    """get seeds list and start producer"""
    print('spider application has started\n')

    cl_args = get_commandline_args()
    print(cl_args)
    seeds = read_file('seeds.json')
    print('readed seeds list\n')
    print("\nCurrent tasks in queue:\n" + "\n".join(
            [seed for seed in seeds]) + "\n\n")
    producer = Producer()
    print('producer starting\n')
    producer.run(seeds)
    print('spider finished working')


def get_commandline_args():
    """parse command line arguments"""
    parser = argparse.ArgumentParser()
    parser.add_argument("-p", "--proxy", default=None,
                        help="specify proxy parameter", nargs='?', const=1, type=str)
    parser.add_argument("-t", "--target", default=None,
                        help="specify target url", nargs='?', const=1, type=str)
    parser.add_argument("-w", "--workers", default=None,
                        help="specify workers pool count", nargs='?', const=1, type=int)
    parser.add_argument("-cl", "--collection_name", default=None,
                        help="specify collection name in database")
    return parser.parse_args()


if __name__ == '__main__':
    main()
