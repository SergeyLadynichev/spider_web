# -*- coding: utf-8 -*-

import queue
import time
import environ
import configparser

from crawler import Crawler
from pymongo import MongoClient

from consumer import Consumer
from helpers import get_cpu_count, singleton, read_file, memoize, profiler
from task import Task

env = environ.Env()
config = configparser.ConfigParser()
config.read('local_env.ini')


@singleton
class Producer:

    def __init__(self):

        # init queues
        self.queue_var_input = queue.Queue()
        self.queue_var_output = queue.Queue()

        # setup settings
        host = env.str("MONGODB_URL", config['LOCAL_ENV']['MONGODB_URL'])
        print('database host {0}'.format(host))
        db_name = env.str("DB_NAME", config['LOCAL_ENV']['DB_NAME'])

        db_client = MongoClient(host)
        db = db_client[db_name]

        # name collection in mongodb
        self.collection = db[time.strftime("%d-%m-%y %H:%M")]

        # max link deep (parents limit)
        self.max_deep = env.int("MAX_DEEP", int(config['LOCAL_ENV']['MAX_DEEP']))

    def build_worker_pool(self, size=get_cpu_count()):
        """return consumer instances pool"""
        crawler = Crawler()
        workers = []

        for _ in range(size):
            worker = Consumer(self.queue_var_input, self.queue_var_output, crawler)
            worker.start()
            workers.append(worker)
        return workers

    @staticmethod
    def create_key(string):
        """format string for mongodb key"""
        return string.replace('.', '-')

    def write_to_storage(self, task):
        """write to storage"""
        print("{}   deep: {}".format(task.url, task.deep))
        links, url = task.links, task.url

        self.collection.insert_one({
            self.create_key(url): links,
            'task': str(task)
        })

    @staticmethod
    def finish_workers(worker_threads):
        """consumer read quit from queue and finishing"""
        for worker in worker_threads:
            worker.frontier_queue.put('quit')

        for worker in worker_threads:
            worker.join()

    @memoize
    def task_generator(self, url, parent_task_deep):
        new_task = Task(url, deep_value=parent_task_deep + 1)
        self.queue_var_input.put(new_task)

    @profiler
    def run(self, tasks):
        """run producer singleton instance"""
        worker_threads = self.build_worker_pool()

        # init firsts tasks from seed.json
        [self.task_generator(url, 0) for url in tasks]

        # read input queue while not empty
        time.sleep(3)
        while True:

            if not self.queue_var_output.empty():
                task = self.queue_var_output.get()
                self.write_to_storage(task)

                if task.check_deep(self.max_deep):
                        # create and put in queue children task for each link in task links
                        for link in task.links:
                            self.task_generator(link, task.deep)

                self.queue_var_output.task_done()

            if self.queue_var_input.empty():
                break

        self.finish_workers(worker_threads)

