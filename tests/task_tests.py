# -*- coding: utf-8 -*-

"""
unittests for spider_web/task.py
"""

import unittest

from ddt import ddt, data

from src.core.task import Task


@ddt
class TestTask(unittest.TestCase):

    @data('bbc.com', 'same_other_url')
    def test_init(self, value):
        task = Task(value)
        self.assertTrue(isinstance(task, Task))
        self.assertTrue(hasattr(task, 'url'))
        self.assertTrue(hasattr(task, 'links'))
        self.assertTrue(hasattr(task, 'deep'))

if __name__ == '__main__':
    unittest.main()
