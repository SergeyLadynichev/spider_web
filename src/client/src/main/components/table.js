import React, { Component } from 'react';

class Table extends React.Component {
    render() {
        const numbers = [1,2,3,4,5];
        const listItems = numbers.map((number) =>
        <td>{number}</td>
        );
        return(
            <table>
                <tr>{listItems}</tr>
                <tr>{listItems}</tr>
                <tr>{listItems}</tr>
            </table>
        )
    }
}


export default Table;
