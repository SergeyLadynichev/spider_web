FROM python:3.5-jessie
RUN apt-get update && apt-get install -y \
    git


RUN mkdir home/developer && mkdir home/developer/core
WORKDIR home/developer/core
ADD ./src/core/requirements.txt ./
RUN pip install -r requirements.txt
